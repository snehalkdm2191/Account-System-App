from django.conf.urls import url
from AccountSystemApp import views

urlpatterns=[
    url(r'^customers/$',views.CustomerApi),
    url(r'^customers/([0-9]+)$',views.CustomerApi)
]
