from django.db import models

# Create your models here.

class Customer(models.Model):
    Id = models.AutoField(primary_key=True)
    AccountNumber = models.CharField(max_length=100)
    Amount = models.CharField(max_length=100)
    Transaction = models.CharField(max_length=100)