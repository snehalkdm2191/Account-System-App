from django.apps import AppConfig


class AccountsystemappConfig(AppConfig):
    name = 'AccountSystemApp'
