from rest_framework import serializers
from AccountSystemApp.models import Customer

class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ('Id',
                  'AccountNumber',
                  'Amount',
                  'Transaction')